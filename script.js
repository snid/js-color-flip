'use strict'

const elem = document.querySelector('body')
const elemDiv = document.querySelector('#div1')
const elemBtn = document.querySelector('#btnChange')
const elemInput1 = document.querySelector('#hidden_color_rgb')
const elemInput2 = document.querySelector('#hidden_color_hex')

// TESTING
// const colorArray = [
//     'red', 'blue', 'green', 'purple', 'coral', 'yellow', 'orange'
// ]

elem.addEventListener('keydown', e => {
    if(e.key === ' ' || e.key === 'Enter' )
    changeColor()
})

const changeColor = () => {
    console.log(elemInput1.value);
    let colorValueHex = ''
    let colorStrArray = []
    // let displayColorRange = [3, 6]       //default
    let displayColorRange = [3, 5, 6, 8]       //just to test
    let displayColorLimit = displayColorRange[Math.floor(Math.random()*displayColorRange.length)]
    
    // let colorValueRGB = {
    //     r: '',
    //     g: '',
    //     b: ''
    // }
    // console.log('inside div1');
    // console.log('displayColorLimit',displayColorLimit );
    for(let i = 0; i < displayColorLimit; i++){
        colorStrArray.push(Math.floor(Math.random()*16).toString(16))
    }
    // console.log(colorStrArray);
    colorValueHex = colorStrArray.join('')
    // console.log(colorValueHex);
    
    // colorValueRGB.r = colorStrArray.slice(0,2).join('')
    // colorValueRGB.r = parseInt(colorValueRGB.r, 16)

    // console.log(colorValueRGB);
    const random = Math.floor(Math.random()*10)
    // elem.style.backgroundColor = colorArray[random]
    elem.style.backgroundColor = `#${colorValueHex}`
    document.querySelector('.color-title').style.color = `#${colorValueHex}`
    document.querySelector('svg').style.backgroundColor = `#${colorValueHex}`
    document.querySelector('.info svg ').style.border = ` 2px solid #${colorValueHex}`
    document.querySelector('.btnChange').style.color = `  #${colorValueHex}`
    // elemBtn.style.backgroundColor = `#${colorValueHex}`
    // console.log(elem.style.backgroundColor);
    document.getElementById('color_rgb').textContent = elem.style.backgroundColor
    document.getElementById('color_hex').textContent = `#${colorValueHex}`
    
    elemInput1.value = `${elem.style.backgroundColor}`
    elemInput2.value = `#${colorValueHex}`

    // changeColorDiv()
}

const copyText = (colorValueType) => {
    let copyText

    console.log(colorValueType);
    if(colorValueType === 'rgb')
        copyText = elemInput1
    if(colorValueType === 'hex')
        copyText = elemInput2

    // console.log(copyText.value);
    // copyText[0].select();
    copyText.select();
    // copyText.setSelectionRange(0, 99999);    //FOR MOBILE DEVICES
    document.execCommand("copy");
}

const changeColorDiv = () => {
    let colorValueDiv = ''
    let colorStrArrayDiv = []
    // console.log('inside div1');
    for(var i = 0; i < 6; i++){
        colorStrArrayDiv.push(Math.floor(Math.random()*17).toString(16))
    }
    // console.log(colorStrArray);
    colorValueDiv = colorStrArrayDiv.join('')
    // console.log(colorValue);
    const random = Math.floor(Math.random()*10)
    // elem.style.backgroundColor = colorArray[random]
    elemDiv.style.backgroundColor = `#${colorValueDiv}`
}